# Iosevka Custom Nerdfont

A nix flake which sets up my customized Iosevka fonts and patches them with Nerdfont patcher.

## License

- [Iosevka License](https://github.com/be5invis/Iosevka/blob/v28.0.1/LICENSE.md)
- [Nerdfont License](https://github.com/ryanoasis/nerd-fonts/blob/v3.1.1/LICENSE)
- The build script itself is distributed under the [Unlicense](https://unlicense.org/)