{
  description = "Customized build of the Iosevka font with Nerd Font patch applied";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=23.11";
    flake-utils.url = "github:numtide/flake-utils";
    iosevka-src = {
      url = "github:be5invis/Iosevka?ref=v28.1.0";
      flake = false;
    };
    font-patcher-src = {
      url = "file+https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/FontPatcher.zip";
      flake = false;
    };
  };

  outputs = inputs @ { self, nixpkgs, flake-utils, iosevka-src, font-patcher-src, ... }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs { inherit system; };
          private-build-plans-version = "1.0.0";
        in
        {
          packages = rec {
            font-patcher = pkgs.stdenv.mkDerivation {
              pname = "font-patcher";
              version = "3.1.1";
              nativeBuildInputs = with pkgs; [ unzip ];
              buildInputs = with pkgs; [ (python3.withPackages (ps: [ ps.fontforge ])) ];
              src = font-patcher-src;
              unpackCmd = "unzip $curSrc -d font-patcher";
              postUnpack = "cp -r font-patcher $out";
              installPhase = ''
                mkdir -p $out/bin
                cp $out/font-patcher $out/bin/nerdfont-patcher
                sed -i 's/bin\/scripts\/name_parser/scripts\/name_parser/g' $out/bin/nerdfont-patcher
              '';
            };
            private-build-plans = pkgs.stdenv.mkDerivation {
              pname = "private-build-plans";
              version = private-build-plans-version;
              src = ./.;
              dontBuild = true;
              installPhase = ''
                mkdir -p $out
                cp private-build-plans.toml $out
              '';
            };
            iosevka-custom = pkgs.buildNpmPackage {
              pname = "iosevka-custom";
              version = private-build-plans-version;
              nativeBuildInputs = with pkgs; [ ttfautohint nodejs_20 private-build-plans ];
              npmDepsHash = "sha256-bzQ7dc7UiC++0DxnQHusu6Ym7rd7GgeA6bGSnnla1nk=";
              src = iosevka-src;
              buildPhase = ''
                cp ${private-build-plans}/private-build-plans.toml .
                npm run build -- ttf::IosevkaCustomCode
                npm run build -- ttf::IosevkaCustomMono
              '';
              enableParallelBuilding = true;
              installPhase = ''
                fontdir="$out/share/fonts/truetype"
                mkdir -p $fontdir
                cp ./dist/IosevkaCustomCode/TTF/* $fontdir
                cp ./dist/IosevkaCustomMono/TTF/* $fontdir
              '';
            };
            iosevka-custom-nerdfont = pkgs.stdenv.mkDerivation {
              pname = "iosevka-custom-nerdfont";
              version = private-build-plans-version;
              src = ./.;
              nativeBuildInputs = [ iosevka-custom font-patcher ];
              buildPhase = ''
                mkdir -p fonts
                find ${iosevka-custom}/share/fonts/truetype -type f -print0 \
                  | xargs -t -n 1 -0 ${font-patcher}/font-patcher --outputdir fonts
              '';
              installPhase = ''
                fontdir="$out/share/fonts/truetype"
                mkdir -p $fontdir
                cp fonts/* $fontdir
              '';
            };
            default = iosevka-custom-nerdfont;
          };
        }
      ) // {
      formatter."x86_64-linux" = nixpkgs.legacyPackages."x86_64-linux".nixpkgs-fmt;
    };
}
